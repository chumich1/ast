class ASTNode:

    def __init__(self):
        self.parent = None 
        self.leftmost_sibling = None
        self.leftmost_child = None
        self.right_sibling = None
        self.__type = "ASTNode"
        self.__value = ""
        self.__print_name = ""
    
    def getPrintName(self):
        return self.__print_name

    def setPrintName(self, new_print_name):
        self.__print_name = new_print_name

    def makeSiblings(self, node_y):
        #nextNode = self.rightSib() # find rightmost
        xsibs = self
        while xsibs.getRightSib() is not None:
            xsibs = xsibs.getRightSib()

        ysibs = node_y.getLeftmostSib()
        xsibs.setRightSib(ysibs)

        ysibs.setLeftmostSib(xsibs.getLeftmostSib())
        ysibs.setParent(xsibs.getParent())
        while ysibs.getRightSib() is not None:
            ysibs = ysibs.getRightSib()
            ysibs.setLeftmostSib(xsibs.getLeftmostSib())
            ysibs.setParent(xsibs.getParent)
        return ysibs
     
    def adoptChildren(self, node_y):

        #If a leftmost child exists, makeSiblings
        if self.leftmost_child is not None:
            self.leftmost_child.makeSiblings(node_y)
        else: # We are at left most sibling
            nextNode = node_y.getLeftmostSib()

            if nextNode is None:
                nextNode = node_y

            self.setLeftmostChild(nextNode)
            while nextNode is not None: #Update all childrens parents
                nextNode.setParent(self)
                nextNode = nextNode.getRightSib()
        """
        while nextNode is not None:
            #moved setParent into makeSiblings
            nextNode = nextNode.rightSib()
        """

    def getType(self):
        return self.__type
    
    def getValue(self):
        return self.__value

    def getRightSib(self):
        return self.right_sibling

    def setRightSib(self, node):
        self.right_sibling = node

    def getLeftmostSib(self):
        if self.leftmost_sibling is None:
            return self
        return self.leftmost_sibling

    def setLeftmostSib(self, node):
        self.leftmost_sibling = node

    def setParent(self, node):
            self.parent = node

    def getParent(self):
            return self.parent

    def getLeftmostChild(self):
        return self.leftmost_child

    def setLeftmostChild(self, node):
        self.leftmost_child = node



class AssignNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "assign"

    def getType(self):
        return self.__type



class IntNode (ASTNode):
    def __init__(self, n):
        super().__init__()
        self.__value = n
        self.__type = "intConst"

    def getType(self):
        return self.__type

    def getValue(self):
        return self.__value

class IdentifierNode (ASTNode): 
    def __init__(self, name):
        super().__init__()
        self.__name = name
        self.__type = "identifier"

    def getType(self):
        return self.__type


class BlockNode (ASTNode): 
    def __init__(self):
        super().__init__()
        self.__type = "Block"

    def getType(self):
        return self.__type

class StartNode (ASTNode): 
    def __init__(self):
        super().__init__()
        self.__type = "Start"

    def getType(self):
        return self.__type

# Bool Op nodes
class BoolEQNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "bool_EQ"

    def getType(self):
        return self.__type


class BoolGTNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "bool_GT"

    def getType(self):
        return self.__type   

class BoolLTNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "bool_LT"

    def getType(self):
        return self.__type        

class BoolGEQNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "bool_GEQ"

    def getType(self):
        return self.__type

class BoolLEQNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "bool_LEQ"

    def getType(self):
        return self.__type

class BoolNEQNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "bool_NEQ"

    def getType(self):
        return self.__type

class IfNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "if"

    def getType(self):
        return self.__type

class ShiftLNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "shift_L"

    def getType(self):
        return self.__type

class ShiftRNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "shift_R"

    def getType(self):
        return self.__type

class PlusNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "plus"

    def getType(self):
        return self.__type

class MinusNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "minus"

    def getType(self):
        return self.__type

class MultNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "mult"

    def getType(self):
        return self.__type

class DivNode(ASTNode):
    def __init__(self):
        super().__init__()
        self.__type = "div"

    def getType(self):
        return self.__type

'''
Helper function to make a happy family
@parent the eagerly awating parent node
@children the new born child waiting to see the world
'''
def makeFamily(parent, children ):
    for child in children:
        parent.adoptChildren(child)


