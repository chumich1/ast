import ply.lex as lex

# lower into UPPER
reserved = {
   'if' : 'IF',
   'else' : 'ELSE',
   'int' : 'INT',
   'const' : 'CONST',
   'return' : 'RETURN'
 
}

notReserved = [
    "COMMA",
    "SEMI",
    "ASSIGN",
    "COMMENT",
    #"ELSE",
    "LCURLY",
    "RCURLY",
    #"RETURN",
    #"IF",
    "LPAREN",
    "RPAREN",
    "PLUS",
    "MINUS",
    "MULT",
    "SHIFTR",
    "SHIFTL",
    "DIV",
    "EQ",
    "GEQ",
    "LEQ",
    "NEQ",
    "GT",
    "LT",
    #"INT",
    #"CONST",
    "NUM",
    "ID"
]

tokens = notReserved + list(reserved.values())

def t_COMMENT(t):    
    r'\/\/.*\n'
    return;

t_COMMA = r"\,"
t_SEMI = r"\;"
t_ASSIGN = r"\="
t_ELSE = r"else" 
t_IF = r"if"
t_RETURN = r"return"
t_LPAREN = r"\("
t_RPAREN = r"\)"
t_PLUS = r"\+"
t_LCURLY = r"\{"
t_RCURLY = r"\}"
t_MINUS = r"\-"
t_MULT = r"\*"
t_DIV = r"\/"
t_SHIFTR = r"\>\>"
t_SHIFTL = r"\<\<"
t_EQ = r"\=\="
t_GEQ = r"\>\="
t_LEQ = r"\<\="
t_GT = r"\>"
t_LT = r"\<"
t_CONST = r'const'
t_INT = r'int'


def t_ID(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'ID')    # Check for reserved words
    return t

def t_NUM(t):
    r'\d+'
    t.value = int(t.value)    
    return t



# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'

def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

# Error handling rule
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

data = ''' 
int x, y = 1 << 2;
const int y = 4+5;
'''
lexer = lex.lex()
"""
# Give the lexer some input
lexer.input(data)

# Tokenize
while True:
    tok = lexer.token()
    if not tok: 
        break      # No more input
    print(tok)
"""